module configman;

import std.array;
import std.conv;
import std.file;
import std.format;
import std.json;

class ConfigManager {
    class Patch {
        struct RawData {
            uint offset;
            ubyte[] patchval;
        }
        struct IntData {
            uint offset;
            long[3] intval;
        }
    public:
        this(string n) {
            name_ = n;
        }
        void addRawPatch(ulong offset, string seq) {
            type_ = 0;
            RawData rd;
            rd.offset = cast(uint)offset;
            auto r = seq.split();
            foreach(s; r) {
                ubyte n;
                s.formattedRead("%x", &n);
                rd.patchval ~= n;
            }
            raw_ ~= rd;
        }
        void setIntPatch(ulong offset, long[3] vals) {
            type_ = 1;
            int_.offset = cast(uint)offset;
            int_.intval = vals;
        }
        @property const(string) name() { return name_; }
        @property const(int) type() { return type_; }
        @property const(RawData[]) rawData() { return raw_; }
        @property const(IntData) intData() { return int_; }

    private:
        string name_;
        int type_;
        RawData[] raw_;
        IntData int_;
    }
    struct PatchSet {
        string name;
        Patch[] patches;
    }
    public this() {
        JSONValue[string] value = parseJSON(to!string(read("modconf.json"))).object;
        foreach(key, val; value) {
            Patch[] pl;
            foreach(v; val.object["patches"].array) {
                auto desc = v["desc"].str;
                auto ptype = v["type"].str;
                auto patch = new Patch(desc);
                foreach(v2; v["patch"].array) {
                    ulong offset;
                    auto ostr = v2["offset"].str;
                    ostr.formattedRead(ostr[0..2] == "0x" ? "0x%x" : "%x", &offset);
                    switch(ptype) {
                    case "raw":
                        patch.addRawPatch(offset, v2["bytes"].str);
                        break;
                    case "int":
                        patch.setIntPatch(offset, [v2["min"].integer, v2["max"].integer, v2["default"].integer]);
                        break;
                    case "radio":
                        break;
                    default:
                        continue;
                    }
                }
                pl ~= patch;
            }
            patches[val.object["size"].integer] = PatchSet(key, pl);
        }
    }
    public PatchSet* getPatch(ulong size) {
        return size in patches;
    }

    private PatchSet[long] patches;
}

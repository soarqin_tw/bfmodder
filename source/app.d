﻿module app;

import mainform;

import core.runtime;
import core.sys.windows.windows;
import std.string;

extern (Windows)
int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
            LPSTR lpCmdLine, int nCmdShow)
{
    int result;
 
    try
    {
        Runtime.initialize();
        result = bfmMain();
        Runtime.terminate();
    }
    catch (Throwable e) 
    {
        MessageBoxA(null, e.toString().toStringz(), "Error",
                    MB_OK | MB_ICONEXCLAMATION);
        result = 0;     // failed
    }
 
    return result;
}
 
int bfmMain() {
    return Application.run(new MainForm()); // Start the application
}

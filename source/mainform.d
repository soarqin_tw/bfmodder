module mainform;

import configman;

import std.conv;
import std.file;
import std.format;

public import dgui.all;
import dgui.layout.gridpanel;
import dgui.layout.panel;

class MainForm: Form {
    class MyCheckBox: CheckBox {
    public:
        this(int idx) {
            index_ = idx;
        }
        @property final int index() {
            return index_;
        }
    private:
        int index_;
    }
    class MyNumberEdit: TextBox {
    public:
        this(int idx) {
            index_ = idx;
            numbersOnly = true;
        }
        @property final int index() {
            return index_;
        }
    private:
        int index_;
    }
    struct PatchState {
        ConfigManager.Patch patch;
        bool enabled;
        int value;
    }
private:
    Panel topPanel_, botPanel_;
    GridPanel gridPanel_;
    Button select_;
    Label filename_;
    Button patch_;
    int wdiff = 0, hdiff = 0;
    int gridCount = 0;
    ConfigManager cfgMan = null;
    PatchState[int] states;
    string fileToPatch;

public:
    this() {
        cfgMan = new ConfigManager();
        text = "Brave Frontier Modder";
        maximizeBox = false;

        formBorderStyle = FormBorderStyle.fixedDialog;
        startPosition = FormStartPosition.centerScreen; // Set Form Position

        RECT r = {100, 100, 100, 100};
        AdjustWindowRectEx(&r, WS_CAPTION | WS_DLGFRAME, false, core.sys.windows.windows.WS_EX_DLGMODALFRAME | core.sys.windows.windows.WS_EX_WINDOWEDGE);
        wdiff = r.right - r.left;
        hdiff = r.bottom - r.top;
        updateGridSize();

        topPanel_ = new Panel();
        topPanel_.parent = this;
        topPanel_.size = Size(100, 40);
        topPanel_.dock = DockStyle.top;

        botPanel_ = new Panel();
        botPanel_.parent = this;
        botPanel_.size = Size(100, 40);
        botPanel_.dock = DockStyle.bottom;

        select_ = new Button();
        select_.parent = topPanel_;
        select_.text = "Open";
        select_.position = Point(8, 8);
        select_.size = Size(60, 24);
        select_.click.attach(&onSelectFile);

        filename_ = new Label();
        filename_.parent = topPanel_;
        filename_.position = Point(76, 8);
        filename_.size = Size(360, 24);

        patch_ = new Button();
        patch_.parent = botPanel_;
        patch_.text = "Patch";
        patch_.position = Point(8, 8);
        patch_.size = Size(80, 24);
        patch_.position = Point((size.width - 80) / 2, 8);
        patch_.enabled = false;
        patch_.click.attach(&onPatchFile);

        gridPanel_ = new GridPanel();
        gridPanel_.parent = this;
        gridPanel_.dock = DockStyle.fill;
    }

private:
    void updateGridSize() {
        auto oldheight = size.height;
        auto oldpos = position;
        size = Size(450 + wdiff, 80 + hdiff + gridCount * 32);
        position = Point(oldpos.x, oldpos.y + oldheight / 2 - size.height / 2);
    }

    void loadPatches(const(string) filename) {
        long size = getSize(filename);
        auto patch = cfgMan.getPatch(size);
        if(patch == null) return;

        int l = gridPanel_.rows.length;
        for(auto i = l - 1; i >= 0; -- i)
            gridPanel_.removeRow(i);
        gridCount = 0;

        filename_.text = "(" ~ patch.name ~ ") " ~ filename;
        foreach(v; patch.patches) {
            auto row = gridPanel_.addRow();
            row.marginTop = 4;
            row.marginBottom = 4;
            row.height = 24;
            auto cb = new MyCheckBox(gridCount);
            states[gridCount] = PatchState(v, false, 0);
            cb.text = v.name;
            cb.click.attach(&onPatchCheck);
            auto col = row.addColumn(cb);
            col.marginLeft = 32;
            switch(v.type) {
            case 0:
                col.width = 400;
                break;
            case 1:
                col.width = 120;
                auto ne = new MyNumberEdit(gridCount);
                ne.maxLength = 5;
                ne.text = format("%d", v.intData.intval[2]);
                ne.textChanged.attach(&onPatchIntChanged);
                col = row.addColumn(ne);
                col.width = 60;
                col.marginLeft = 8;
                auto lb = new Label();
                lb.text = format("(%d-%d)", v.intData.intval[0], v.intData.intval[1]);
                col = row.addColumn(lb);
                col.width = 80;
                col.marginLeft = 2;
                break;
            case 2:
                break;
            default:
                break;
            }
            ++ gridCount;
        }
        updateGridSize();
        fileToPatch = filename;
        patch_.enabled = true;
    }

    void onSelectFile(Control sender, EventArgs e)
    {
        auto browse = new FileBrowserDialog();
        browse.filter = "libgame.so|libgame.so";
        if(browse.showDialog()) {
            loadPatches(browse.result);
        }
    }

    void onPatchFile(Control sender, EventArgs e)
    {
        auto fileContent = cast(ubyte[])read(fileToPatch);
        foreach(stat; states) {
            if(!stat.enabled) continue;
            switch(stat.patch.type) {
            case 0:
                foreach(data; stat.patch.rawData) {
                    fileContent[data.offset..data.offset+data.patchval.length] = data.patchval;
                }
                break;
            case 1:
                auto data = stat.patch.intData;
                if(stat.value < 256) {
                    fileContent[data.offset..data.offset+4] = [cast(ubyte)stat.value, 0x20, 0x70, 0x47];
                } else if(stat.value <= 5000) {
                    fileContent[data.offset..data.offset+8] = [0x14, 0x20, cast(ubyte)(stat.value / 20), 0x23, 0x58, 0x43, 0x70, 0x47];
                } else if(stat.value <= 20000) {
                    fileContent[data.offset..data.offset+8] = [0x64, 0x20, cast(ubyte)(stat.value / 100), 0x23, 0x58, 0x43, 0x70, 0x47];
                } else if(stat.value <= 60000) {
                    fileContent[data.offset..data.offset+8] = [0xFA, 0x20, cast(ubyte)(stat.value / 250), 0x23, 0x58, 0x43, 0x70, 0x47];
                }
                break;
            case 2:
                break;
            default:
                break;
            }
        }
        write(fileToPatch, fileContent);
        MsgBox.show("Brave Frontier Modder", "Patch applied.");
    }

    void onPatchCheck(Control sender, EventArgs e)
    {
        auto cb = cast(MyCheckBox)sender;
        auto ps = &states[cb.index];
        ps.enabled = cb.checked;
    }

    void onPatchIntChanged(Control sender, EventArgs e)
    {
        auto ne = cast(MyNumberEdit)sender;
        auto ps = &states[ne.index];
        int val;
        string s = ne.text;
        s.formattedRead("%d", &val);
        if(val < ps.patch.intData.intval[0]) {
            ps.value = cast(int)ps.patch.intData.intval[0];
            ne.text = format("%d", ps.value);
        } else if(val > ps.patch.intData.intval[1]) {
            ps.value = cast(int)ps.patch.intData.intval[1];
            ne.text = format("%d", ps.value);
        } else
            ps.value = val;
    }
}
